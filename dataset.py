import numpy as np
from PIL import Image
import random
import h5py


class tDShape:
    def __init__(self, root, train=True, transform=None, nocolour=False):
        super(tDShape, self).__init__()
        self.transform = transform
        self.train = train  # training set or test set

        self.FACTORS_IN_ORDER = ['floor_hue', 'wall_hue', 'object_hue', 'scale', 'shape',
                     'orientation']
        self.NUM_VALUES_PER_FACTOR = {'floor_hue': 10, 'wall_hue': 10, 'object_hue': 10, 
                          'scale': 8, 'shape': 4, 'orientation': 15}

        print('loading data...')
        dataset = h5py.File(root,'r')
        images = dataset['images']
        labels = dataset['labels']

        if self.train:
            images = images[:400000,:]
            labels = labels[:400000,:]
        else:
            images = images[400001:,:]
            labels = labels[400001:,:]
        self.images = images
        self.labels = labels
        self.nocolour=nocolour

        print('data loaded.')

    def get_index(self,factors):
        """ Converts factors to indices in range(num_data)
        Args:
            factors: np array shape [6,batch_size].
             factors[i]=factors[i,:] takes integer values in 
             range(_NUM_VALUES_PER_FACTOR[_FACTORS_IN_ORDER[i]]).

        Returns:
            indices: np array shape [batch_size].
        """
        indices = 0
        base = 1
        for factor, name in reversed(list(enumerate(self.FACTORS_IN_ORDER))):
            indices += factors[factor] * base
            base *= self.NUM_VALUES_PER_FACTOR[name]
        return indices

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        """
        #generate transform parameters
        rand_index=np.array([0,400000])
        if self.train:
            while (rand_index>=400000).any():
                rand_floor=np.random.choice(self.NUM_VALUES_PER_FACTOR['floor_hue'],2)
                rand_wall=np.random.choice(self.NUM_VALUES_PER_FACTOR['wall_hue'],2)
                rand_object=np.random.choice(self.NUM_VALUES_PER_FACTOR['object_hue'],2)

                if self.nocolour:
                    rand_floor[1], rand_wall[1], rand_object[1]=rand_floor[0], rand_wall[0], rand_object[0]#only use the non-colour ones, i.e. three transformations

                rand_scale=np.random.choice(self.NUM_VALUES_PER_FACTOR['scale'],2)
                rand_shape=np.random.choice(self.NUM_VALUES_PER_FACTOR['shape'],2)
                rand_orient=np.random.choice(self.NUM_VALUES_PER_FACTOR['orientation'],2)
                factors=np.stack((rand_floor,rand_wall,rand_object,rand_scale,rand_shape,rand_orient))
                rand_index=self.get_index(factors)
        else:
            while (rand_index<400000).any():
                rand_floor=np.random.choice(self.NUM_VALUES_PER_FACTOR['floor_hue'],2)
                rand_wall=np.random.choice(self.NUM_VALUES_PER_FACTOR['wall_hue'],2)
                rand_object=np.random.choice(self.NUM_VALUES_PER_FACTOR['object_hue'],2)

                if self.nocolour:
                    rand_floor[1], rand_wall[1], rand_object[1]=rand_floor[0], rand_wall[0], rand_object[0]#only use the non-colour ones, i.e. three transformations


                rand_scale=np.random.choice(self.NUM_VALUES_PER_FACTOR['scale'],2)
                rand_shape=np.random.choice(self.NUM_VALUES_PER_FACTOR['shape'],2)
                rand_orient=np.random.choice(self.NUM_VALUES_PER_FACTOR['orientation'],2)
                factors=np.stack((rand_floor,rand_wall,rand_object,rand_scale,rand_shape,rand_orient))
                rand_index=self.get_index(factors)
            rand_index-=400001

        #source image and params
        img = self.images[rand_index[0]]
        params_source = factors[:,0]

        #target image and params
        img_trans = self.images[rand_index[1]]
        params_trans = factors[:,1]

        params = params_trans-params_source

        if self.nocolour:
            params=params[3:]#only us non-colour ones

        # to return a PIL Image
        img = Image.fromarray(img)
        img_trans = Image.fromarray(img_trans)

        if self.transform is not None:
            img = self.transform(img)
            img_trans = self.transform(img_trans)

        return img, img_trans, params

    def __len__(self):
        return len(self.labels)