import os
import torch
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
import numpy as np
from tqdm import tqdm
import random

import piq
from dataset import tDShape
from models import model_product, model_additive

torch.manual_seed(0)
np.random.seed(0)

# setting='Product'
setting='Additive'

if not os.path.exists('./out_img/pred_'+setting):
    os.mkdir('./out_img/pred_'+setting)

def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 3, 96, 96)
    return x

batch_size = 1
num_img_store = 100#number of images to store for visulisation

img_transform = transforms.Compose([
    transforms.Resize((96,96)),
    transforms.ToTensor(),
    transforms.Normalize((0.5,), (0.5,))
])

dataset = tDShape("3dshapes.h5", train=False, transform=img_transform,nocolour=False)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=False)

#loading model according to settings
if setting == 'Product':
    model = model_product().cuda()
    pretrained = torch.load('Product.pth')
elif setting == 'Additive':
    model = model_additive().cuda()
    pretrained = torch.load('Additive.pth')
model.load_state_dict(pretrained)
model.eval()

#start testing
psnr_all=0
ssim_all=0
seed=np.random.randint(1212345)
for i, data in enumerate(tqdm(dataloader)):
    random.seed(seed)

    img, img_trans, param = data
    img = Variable(img).cuda()
    img_trans = Variable(img_trans).cuda()
    param = Variable(param).cuda()
    # ===================forward=====================
    output = model(img,param)
    # ===================PSNR/SSIM=====================
    psnr=piq.psnr(to_img(output.cpu().data), to_img(img_trans.cpu().data))
    ssim=piq.ssim(to_img(output.cpu().data), to_img(img_trans.cpu().data))
    psnr_all+=psnr
    ssim_all+=ssim

    if i<num_img_store:#store images
        pred=to_img(output.cpu().data)
        save_image(pred,'./out_img/pred_'+setting+'/{}_pred.png'.format(i))
        GT=to_img(img_trans.cpu().data)
        save_image(GT,'./out_img/pred_'+setting+'/{}_GT.png'.format(i))
        input=to_img(img.cpu().data)
        save_image(input,'./out_img/pred_'+setting+'/{}_input.png'.format(i))

print("PSNR: %.2f"%(psnr_all/len(dataloader)))
print("SSIM: %.4f"%(ssim_all/len(dataloader)))
