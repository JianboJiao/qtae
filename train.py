import os
from tqdm import tqdm
import torch
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image, make_grid
from tensorboardX import SummaryWriter

from dataset import tDShape
from models import model_additive, model_product



# setting='Product'
setting='Additive'

if not os.path.exists('./out_img/'+setting):
    os.mkdir('./out_img/'+setting)

def to_img(x):
    x = 0.5 * (x + 1)
    x = x.clamp(0, 1)
    x = x.view(x.size(0), 3, 96, 96)
    return x
writer = SummaryWriter('./runs_{}'.format(setting))

num_epochs = 100
batch_size = 128
learning_rate = 1e-5

img_transform = transforms.Compose([
    transforms.Resize((96,96)),
    transforms.ToTensor(),
    transforms.Normalize((0.5,), (0.5,))
])


#data loader
dataset = tDShape("./3dshapes.h5", train=True, transform=img_transform,nocolour=False)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

#model define
if setting == 'Product':
    model = model_product().cuda()
elif setting == 'Additive':
    model = model_additive().cuda()
#loss
criterion = nn.SmoothL1Loss()
#optimiser
optimizer = torch.optim.Adam(
    model.parameters(), lr=learning_rate, weight_decay=1e-5)


#training starts
for epoch in range(num_epochs+1):
    for batch_idx, data in enumerate(tqdm(dataloader)):
        img, img_trans, params = data
        img = Variable(img).cuda()
        img_trans = Variable(img_trans).cuda()
        params = Variable(params).cuda()
        # ===================forward=====================
        output = model(img,params)
        loss = criterion(output, img_trans)
        # ===================backward====================
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        # ===================log========================
        if (batch_idx+1)%100==0:
            n_it=epoch*len(dataloader)+batch_idx
            writer.add_scalar('data/loss',loss.item(),n_it)
            each_row=8
            x=make_grid(to_img(output.cpu().data),nrow=each_row)
            writer.add_image('images/output',x,n_it)
            x=make_grid(to_img(img.cpu().data),nrow=each_row)
            writer.add_image('images/input',x,n_it)
            x=make_grid(to_img(img_trans.cpu().data),nrow=each_row)
            writer.add_image('images/target',x,n_it)
    print('epoch [{}/{}], loss:{:.4f}'
          .format(epoch + 1, num_epochs, loss.item()))
    if epoch % 10 == 0:
        #save sample images
        pic = to_img(output.cpu().data)
        save_image(pic, './out_img/'+setting+'/image_{}.png'.format(epoch))
        #save model
        torch.save(model.state_dict(), setting+'.pth')
