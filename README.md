# Quantised Transforming Auto-Encoders: Achieving Equivariance to Arbitrary Transformations in Deep Networks

By [Jianbo Jiao](https://jianbojiao.com/) and [João F. Henriques](https://www.robots.ox.ac.uk/~joao/)

[Paper](https://jianbojiao.com/pdfs/bmvc21_qtae.pdf) | [Project page](https://www.robots.ox.ac.uk/~vgg/research/qtae/) | [Video presentation](https://youtu.be/3Vm5gwfWTro)


### Introduction

This repository contains the implementation for the work described in the paper "[Quantised Transforming Auto-Encoders: Achieving Equivariance to Arbitrary Transformations in Deep Networks](https://www.robots.ox.ac.uk/~vgg/research/qtae/bmvc21_qtae.pdf)". In this work, we teach networks to predict what an image would look like under a transformation, such as rotation and scale, but also much more general changes such as 3D rotations, object deformations and lighting changes.

![idea](idea.gif)

### Usage

0. The project was implemented and tested with Python 3.7, [PyTorch](https://pytorch.org) (version 1.7.1) and [TorchVision](https://pytorch.org/docs/0.2.0/) (version 0.8.2) on Linux with GPUs. Please setup the environment according to the [instructions](https://pytorch.org/get-started/previous-versions/) first. You may also need to install the required packages, e.g. [einops](https://github.com/arogozhnikov/einops), [piq](https://github.com/photosynthesis-team/piq), [tensorboardX](https://github.com/lanpa/tensorboardX), etc.
1. Download the DeepMind 3D Shapes dataset from the official [website](https://github.com/deepmind/3d-shapes) and place it (`3dshapes.h5`) under this folder.
2. Run the [train.py](train.py) to train the model, the trained model will be stored under this folder, and example results will be stored under a folder `out_img`. You may also want to use the tensorboard to visualise the training logs.
3. After training, you could use the [test.py](test.py) to evaluate the performance of the proposed approach. You can optionally set the number of images to store locally for visualisation by setting the `num_img_store` parameter (default set to 100), the output images will also be stored under the `out_img` folder. The PSNR/SSIM quantitative metrics will be reported as well.


### Citation

If you find this project useful, please consider citing:

	@InProceedings{jiao2021qtae,
    author={Jianbo Jiao and João F. Henriques},
    title={Quantised Transforming Auto-Encoders: Achieving Equivariance to Arbitrary Transformations in Deep Networks},
    booktitle = {BMVC},
    year = {2021}
    }
**Reference**

[1] Geoffrey E. Hinton, Alex Krizhevsky, and Sida D. Wang. [Transforming Auto-Encoders](http://www.cs.toronto.edu/~bonner/courses/2020s/csc2547/papers/capsules/transforming-autoencoders,-hinton,-icann-2011.pdf). In International Conference on Artificial Neural Networks 2011.

   
Please email the [authors](mailto:jiaojianbo.i@gmail.com or jianbo@robots.ox.ac.uk) if you have any questions.