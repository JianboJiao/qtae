import torch
from torch import nn
from einops.layers.torch import Rearrange

class model_product(nn.Module):
    def __init__(self):
        super(model_product, self).__init__()

        bn_chnl=64
        feat_size=20
        midfc_size=2048

        self.encode=nn.Sequential(
            nn.Conv2d(3, 16, 5, stride=1, padding=0), # b, 16, 92, 92
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1),#b,16,46,46
            nn.Conv2d(16, 32, 3, stride=1, padding=0), # b, 32, 44, 44
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1),#b,16,22,22
            nn.Conv2d(32, 64, 3, stride=1, padding=0), # b, 64, 20, 20
            nn.ReLU(True),
            nn.Conv2d(64, bn_chnl, 3, stride=1, padding=1),  # b, bn_chnl, 20, 20
        )
        self.decode=nn.Sequential(
            nn.ConvTranspose2d(bn_chnl, 64, 3, stride=1, padding=0),  # b, 64, 22, 22
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 32, 3, stride=2, padding=0),  # b, 32, 45, 45
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 16, 4, stride=2, padding=0),  # b, 16, 92, 92
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 3, 5, stride=1, padding=0), # b, 3, 96, 96
        )

        #transformation parameters dimension
        p_floor=10#floor colour
        p_wall= 10#wall colour
        p_obj= 10#object colour
        p_scl= 8#scale
        p_shape= 4#shape
        p_orien= 15#orientation
        p_others=10
        self.disc=nn.Sequential(#discritising
            Rearrange('b c h w -> b (c h w)'),
            nn.Linear(bn_chnl*feat_size*feat_size, midfc_size),
            nn.ReLU(True),
            #projection to a larger embedding space
            nn.Linear(midfc_size, p_floor*p_wall*p_obj*p_scl*p_shape*p_orien * p_others),#10*10*10*8*4*15=480000
            #rearrange the embedding to fit the target n-dim tensor (here n=6+1)
            Rearrange('b (floor wall obj scl shape orien oth) -> b floor wall obj scl shape orien oth', floor=p_floor, wall=p_wall, obj=p_obj, scl=p_scl, shape=p_shape, orien=p_orien, oth=p_others),
        )
        self.undisc=nn.Sequential(#un-discritising
            Rearrange('b floor wall obj scl shape orien oth -> b (floor wall obj scl shape orien oth)'),
            nn.Linear(p_floor*p_wall*p_obj*p_scl*p_shape*p_orien * p_others, midfc_size),
            nn.ReLU(True),
            nn.Linear(midfc_size, bn_chnl*feat_size*feat_size),
            Rearrange('b (c h w) -> b c h w', c=bn_chnl, h=feat_size, w=feat_size),
        )

    def forward(self, x, params):
        x_enc = self.encode(x)
        #convert to discrete embedding space
        x_disc=self.disc(x_enc)
        #shift/roll the n-D tensor
        para_floor=params[:,0]
        para_wall=params[:,1]
        para_obj=params[:,2]
        para_scl=params[:,3]
        para_shape=params[:,4]
        para_orien=params[:,5]

        for bs in range(x_disc.size(0)):
            #cfloor
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_floor[bs]),dims=0)
            if para_floor[bs]>=0:
                x_disc[bs,:para_floor[bs],:]=0
            else:
                x_disc[bs, para_floor[bs]:,:]=0
            #cwall
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_wall[bs]),dims=1)
            if para_wall[bs]>=0:
                x_disc[bs,:,:para_wall[bs],:]=0
            else:
                x_disc[bs, :,para_wall[bs]:,:]=0
            #cobject
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_obj[bs]),dims=2)
            if para_obj[bs]>=0:
                x_disc[bs,:,:,:para_obj[bs],:]=0
            else:
                x_disc[bs, :,:,para_obj[bs]:,:]=0
            #scale
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_scl[bs]),dims=3)
            if para_scl[bs]>=0:
                x_disc[bs,:,:,:,:para_scl[bs],:]=0
            else:
                x_disc[bs, :,:,:,para_scl[bs]:,:]=0
            #shape
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_shape[bs]),dims=4)
            if para_shape[bs]>=0:
                x_disc[bs,:,:,:,:,:para_shape[bs],:]=0
            else:
                x_disc[bs, :,:,:,:,para_shape[bs]:,:]=0
            #orientation
            x_disc[bs,:]=torch.roll(x_disc[bs,:],shifts=int(para_orien[bs]),dims=5)
            if para_orien[bs]>=0:
                x_disc[bs,:,:,:,:,:,:para_orien[bs], :]=0
            else:
                x_disc[bs, :,:,:,:,:,para_orien[bs]:, :]=0

        #convert back to the continuous space
        x=self.undisc(x_disc)
        x = self.decode(x)

        return x


class model_additive(nn.Module):
    def __init__(self):
        super(model_additive, self).__init__()

        bn_chnl=64
        feat_size=20
        midfc_size=2048

        self.encode=nn.Sequential(
            nn.Conv2d(3, 16, 5, stride=1, padding=0), # b, 16, 92, 92
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1),#b,16,46,46
            nn.Conv2d(16, 32, 3, stride=1, padding=0), # b, 32, 44, 44
            nn.ReLU(True),
            nn.MaxPool2d(kernel_size=3,stride=2,padding=1),#b,16,22,22
            nn.Conv2d(32, 64, 3, stride=1, padding=0), # b, 64, 20, 20
            nn.ReLU(True),
            nn.Conv2d(64, bn_chnl, 3, stride=1, padding=1),  # b, bn_chnl, 20, 20
        )
        self.decode=nn.Sequential(
            nn.ConvTranspose2d(bn_chnl, 64, 3, stride=1, padding=0),  # b, 64, 22, 22
            nn.ReLU(True),
            nn.ConvTranspose2d(64, 32, 3, stride=2, padding=0),  # b, 32, 45, 45
            nn.ReLU(True),
            nn.ConvTranspose2d(32, 16, 4, stride=2, padding=0),  # b, 16, 92, 92
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 3, 5, stride=1, padding=0), # b, 3, 96, 96
        )

        #transformation parameters dimension
        p_floor=10#floor colour
        p_wall= 10#wall colour
        p_obj= 10#object colour
        p_scl= 8#scale
        p_shape= 4#shape
        p_orien= 15#orientation
        p_others=10
        self.disc=nn.Sequential(#discritising
            Rearrange('b c h w -> b (c h w)'),
            nn.Linear(bn_chnl*feat_size*feat_size, midfc_size),
            nn.ReLU(True),
            nn.Linear(midfc_size, p_floor+p_wall+p_obj+p_scl+p_shape+p_orien+p_others),#10+10+10+8+4+15=57
        )
        self.undisc=nn.Sequential(#un-discritising
            nn.Linear(p_floor+p_wall+p_obj+p_scl+p_shape+p_orien+p_others, midfc_size),
            nn.ReLU(True),
            nn.Linear(midfc_size, bn_chnl*feat_size*feat_size),
            Rearrange('b (c h w) -> b c h w', c=bn_chnl, h=feat_size, w=feat_size),
        )

    def forward(self, x, params):
        x_enc = self.encode(x)
        #convert to discrete space
        x_disc=self.disc(x_enc)

        para_floor=params[:,0].int()
        para_wall=params[:,1].int()
        para_obj=params[:,2].int()
        para_scl=params[:,3].int()
        para_shape=params[:,4].int()
        para_orien=params[:,5].int()

        p_floor=10
        p_wall= 10
        p_obj= 10
        p_scl= 8
        p_shape= 4
        p_orien= 15
        p_others=10

        dim_floor=x_disc[:,:p_floor]
        dim_wall=x_disc[:,p_floor:p_floor+p_wall]
        dim_obj=x_disc[:,p_floor+p_wall:p_floor+p_wall+p_obj]
        dim_scl=x_disc[:,p_floor+p_wall+p_obj:p_floor+p_wall+p_obj+p_scl]
        dim_shape=x_disc[:,p_floor+p_wall+p_obj+p_scl:p_floor+p_wall+p_obj+p_scl+p_shape]
        dim_orien=x_disc[:,p_floor+p_wall+p_obj+p_scl+p_shape:p_floor+p_wall+p_obj+p_scl+p_shape+p_orien]

        for bs in range(x_disc.size(0)):
            #cfloor
            dim_floor[bs,:]=torch.roll(dim_floor[bs,:],shifts=int(para_floor[bs]),dims=0)
            if para_floor[bs]>=0:
                dim_floor[bs,:para_floor[bs]]=0
            else:
                dim_floor[bs, para_floor[bs]:]=0
            #cwall
            dim_wall[bs,:]=torch.roll(dim_wall[bs,:],shifts=int(para_wall[bs]),dims=0)
            if para_wall[bs]>=0:
                dim_wall[bs,:para_wall[bs]]=0
            else:
                dim_wall[bs, para_wall[bs]:]=0
            #cobject
            dim_obj[bs,:]=torch.roll(dim_obj[bs,:],shifts=int(para_obj[bs]),dims=0)
            if para_obj[bs]>=0:
                dim_obj[bs,:para_obj[bs]]=0
            else:
                dim_obj[bs, para_obj[bs]:]=0

            #scale
            dim_scl[bs,:]=torch.roll(dim_scl[bs,:],shifts=int(para_scl[bs]),dims=0)
            if para_scl[bs]>=0:
                dim_scl[bs,:para_scl[bs]]=0
            else:
                dim_scl[bs, para_scl[bs]:]=0
            #shape
            dim_shape[bs,:]=torch.roll(dim_shape[bs,:],shifts=int(para_shape[bs]),dims=0)
            if para_shape[bs]>=0:
                dim_shape[bs,:para_shape[bs]]=0
            else:
                dim_shape[bs, para_shape[bs]:]=0
            #orientation
            dim_orien[bs,:]=torch.roll(dim_orien[bs,:],shifts=int(para_orien[bs]),dims=0)
            if para_orien[bs]>=0:
                dim_orien[bs,:para_orien[bs]]=0
            else:
                dim_orien[bs, para_orien[bs]:]=0

        #convert back to the continuous space
        x=self.undisc(x_disc)
        x = self.decode(x)

        return x

